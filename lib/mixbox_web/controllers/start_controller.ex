defmodule MixboxWeb.StartController do
  use MixboxWeb, :controller
  alias Mixbox.Trivia

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def create(conn, _params) do
    {:ok, game} = Trivia.start_game()
    redirect(conn, to: Routes.game_show_path(MixboxWeb.Endpoint, :show, game.id))
  end

  def join(conn, _params) do
    render(conn, "join.html")
  end
end
