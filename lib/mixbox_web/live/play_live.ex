defmodule MixboxWeb.PlayLive do
  use MixboxWeb, :live_view

  alias Mixbox.Trivia

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, joined: false)}
  end

  @impl true
  def handle_params(%{}, _url, socket) do
    {:noreply,
     socket
     |> assign(:page_title, "Join Game")}
  end

  @impl true
  def handle_event("join", %{"game_code" => code, "name" => name}, socket) do
    game = Trivia.find_game_by_code(code)

    case game do
      nil ->
        {:noreply,
         socket
         |> put_flash(:error, "Game not found.")}

      game ->
        broadcast_join(game.id, name)
        {:noreply, assign(socket, %{joined: true, game_id: game.id})}
    end
  end

  @impl true
  def handle_event("start_game", _params, socket) do
    game = Trivia.get_game!(socket.assigns.game_id)

    broadcast_start(game.id)
    {:noreply, socket}
  end

  defp broadcast_join(game_id, player_name) do
    Phoenix.PubSub.broadcast(
      Mixbox.PubSub,
      "game:#{game_id}",
      {:player_join, %{name: player_name}}
    )
  end

  defp broadcast_start(game_id) do
    Phoenix.PubSub.broadcast(
      Mixbox.PubSub,
      "game:#{game_id}",
      :start_button_pressed
    )
  end
end
