defmodule MixboxWeb.GameLive.Show do
  use MixboxWeb, :live_view

  alias Mixbox.Trivia

  @impl true
  def mount(params, _session, socket) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(Mixbox.PubSub, "game:#{params["id"]}")
    end

    game = Trivia.get_game!(params["id"])

    if game.status != "waiting_for_players" do
      {:ok,
       push_redirect(socket,
         to: Routes.run_trivia_game_path(socket, MixboxWeb.TriviaLive, game.id)
       )}
    else
      {:ok,
       socket
       |> assign(:players, Trivia.players_for_game(params["id"]))
       |> assign(:game, game)}
    end
  end

  @impl true
  def handle_info({:player_join, %{name: name}}, socket) do
    # TODO: Ensure a second player can't take over the first player's session Trivia.add_player_to_game(socket.assigns.game.id, name)
    {:noreply,
     socket
     |> assign(players: Trivia.players_for_game(socket.assigns.game.id))}
  end

  @impl true
  def handle_info(:start_button_pressed, socket) do
    game = socket.assigns.game
    {:ok, %Mixbox.Trivia.Game{}} = Trivia.run_game(game)

    {:noreply,
     push_redirect(socket,
       to: Routes.run_trivia_game_path(socket, MixboxWeb.TriviaLive, game.id)
     )}
  end

  defp page_title(:show), do: "Show Game"
  defp page_title(:edit), do: "Edit Game"
end
