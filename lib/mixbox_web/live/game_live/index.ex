defmodule MixboxWeb.GameLive.Index do
  use MixboxWeb, :live_view

  alias Mixbox.Trivia
  alias Mixbox.Trivia.Game

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :games, fetch_games())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Game")
    |> assign(:game, Trivia.get_game!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Game")
    |> assign(:game, %Game{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Games")
    |> assign(:game, nil)
  end

  defp apply_action(socket, :join, _params) do
    socket
    |> assign(:page_title, "Join Game")
    |> assign(:game, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    game = Trivia.get_game!(id)
    {:ok, _} = Trivia.delete_game(game)

    {:noreply, assign(socket, :games, fetch_games())}
  end

  defp fetch_games do
    Trivia.list_games()
  end
end
