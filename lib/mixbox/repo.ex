defmodule Mixbox.Repo do
  use Ecto.Repo,
    otp_app: :mixbox,
    adapter: Ecto.Adapters.Postgres
end
