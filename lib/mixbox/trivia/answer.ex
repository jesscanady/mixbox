defmodule Mixbox.Trivia.Answer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "answers" do
    belongs_to :question, Mixbox.Trivia.Question
    field :answer_text, :string
    field :correct, :boolean
    timestamps()
  end

  def changeset(answer, attrs \\ %{}) do
    answer
    |> cast(attrs, [:answer_text, :question_id])
    |> validate_required([:answer_text, :question_id])
  end
end
