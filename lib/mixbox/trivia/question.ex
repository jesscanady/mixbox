defmodule Mixbox.Trivia.Question do
  use Ecto.Schema
  import Ecto.Changeset

  schema "questions" do
    field :question_text, :string
    field :index, :integer
    has_many :answers, Mixbox.Trivia.Answer, on_delete: :delete_all
    timestamps()
  end

  def changeset(question, attrs \\ %{}) do
    question
    |> cast(attrs, [:index, :question_text])
    |> validate_required(:index)
  end
end
