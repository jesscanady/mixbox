defmodule Mixbox.Trivia.Game do
  use Ecto.Schema
  import Ecto.Changeset

  schema "games" do
    field :code, :string
    field :status, :string, default: "waiting_for_players"
    has_many :players, Mixbox.Trivia.Player
    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [:code, :status])
    |> validate_required([:code, :status])
  end
end
