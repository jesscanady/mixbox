# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :mixbox,
  ecto_repos: [Mixbox.Repo]

# Configures the endpoint
config :mixbox, MixboxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "04t6pSsdPczUv4K3XqL9X7GoP80+m3nT+8kF94+t6CVqZLDAkOE6Nm+Yr1NtHSpx",
  render_errors: [view: MixboxWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Mixbox.PubSub,
  live_view: [signing_salt: "UNUwXLWh"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
