# Mixbox

Browser-based games controlled by individual devices, powered by Phoenix LiveView!

## Development TODOs

* Rename game show liveview to something better.
* GameLive.show -- load game with players rather than two separate socket keys
* Actually show a question for a running game
* Redirect to the running game page if the game is running
* Remove unused StartController/join function and templates and views and stuff.
* Use the route helpers to get the "play game" URL rather than hardcoding it into the view.
* Ensure a second player can't take over the first player's session

## Completed TODOs

* Install `ex_machina` so we can use factories in `trivia_test.ex`
* When we press the start button, why does the game display not auto-update? (it does now)
* Get the pending test running correctly (randomize game codes)

## Deployment?

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).
