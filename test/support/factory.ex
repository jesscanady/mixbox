defmodule Mixbox.Factory do
  use ExMachina.Ecto, repo: Mixbox.Repo

  def game_factory do
    %Mixbox.Trivia.Game{
      code: sequence(:name, &"AAA#{&1}"),
      status: "waiting_for_players"
    }
  end

  def question_factory do
    %Mixbox.Trivia.Question{
      question_text: sequence(:n, &"This is question #{&1}"),
      index: sequence(:index, &"#{&1}")
    }
  end

  def answer_factory do
    %Mixbox.Trivia.Answer{
      question: build(:question),
      answer_text: sequence(:n, &"Answer #{&1}"),
      correct: false
    }
  end
end
