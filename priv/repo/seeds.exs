# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Mixbox.Repo.insert!(%Mixbox.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Mixbox.Repo
alias Mixbox.Trivia.Question
alias Mixbox.Trivia.Answer

question =
  %Question{}
  |> Question.changeset(%{question_text: "Which of these planets is hottest?"})
  |> Repo.insert!()

Repo.insert!(%Answer{answer_text: "Venus", question_id: question.id, correct: true})
Repo.insert!(%Answer{answer_text: "Mars", question_id: question.id})
Repo.insert!(%Answer{answer_text: "Jupiter", question_id: question.id})
Repo.insert!(%Answer{answer_text: "Earth", question_id: question.id})

question =
  %Question{}
  |> Question.changeset(%{question_text: "Who has been a game show host the longest?"})
  |> Repo.insert!()

Repo.insert!(%Answer{answer_text: "Pat Sajak", question_id: question.id})
Repo.insert!(%Answer{answer_text: "Bob Barker", question_id: question.id})
Repo.insert!(%Answer{answer_text: "Alex Trebek", question_id: question.id, correct: true})
Repo.insert!(%Answer{answer_text: "Steve Harvey", question_id: question.id})
