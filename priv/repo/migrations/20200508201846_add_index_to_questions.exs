defmodule Mixbox.Repo.Migrations.AddIndexToQuestions do
  use Ecto.Migration

  def change do
    alter table(:questions) do
      add :index, :integer
    end
  end
end
