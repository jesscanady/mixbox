defmodule Mixbox.Repo.Migrations.CreateQuestions do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :question_text, :string

      timestamps()
    end
  end
end
