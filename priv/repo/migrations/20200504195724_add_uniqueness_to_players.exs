defmodule Mixbox.Repo.Migrations.AddUniquenessToPlayers do
  use Ecto.Migration

  def change do
    unique_index(:players, [:name, :game_id])
  end
end
